import { Injectable } from "@angular/core";
import { Http,Response} from "@angular/http";
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {ProductInfo} from '../modal/prodata';
import {Fruitsuser} from '../modal/userfruits';
import {Usricecream} from '../modal/ice';

@Injectable()
export class GetProductServices{
 private pro ="../../assets/producinfo.json";
 private fru ="../../assets/userproduct.json";
 private cc ="../../assets/icecream.json";
    constructor(private http:Http){}

    pdrinfo(){
        return this.http.get(this.pro)
        .map((res:Response)=> res.json());
    }

    fruit(){
        return this.http.get(this.fru)
        .map((res: Response)=> res.json());
    }

    cream(){
        return this.http.get(this.cc)
        .map((res: Response)=> res.json());
    }

    CurrentProduct(id: number): Observable<ProductInfo>
    {
        return this.http.get(this.pro)
        .map((res: Response)=>
    {
        var productInfoJson= res.json();
        var currentpro = productInfoJson.find((item:ProductInfo)=> item.productid ==id);
        return currentpro;
    })
    }

    CurrentProduct1(id: number): Observable<Fruitsuser>
    {
        return this.http.get(this.fru)
        .map((res: Response)=>
    {
        var fruitsuserJson= res.json();
        var currentpro1 = fruitsuserJson.find((item1:Fruitsuser)=> item1.productid ==id);
        return currentpro1;
    })
    }

    CurrentProduct2(id: number): Observable<Usricecream>
    {
        return this.http.get(this.cc)
        .map((res: Response)=>
    {
        var usricecreamJson= res.json();
        var currentpro2 = usricecreamJson.find((item2:Usricecream)=> item2.productid ==id);
        return currentpro2;
    })
    }
}