import { Component, OnInit } from '@angular/core';
import { Fruitsuser } from '../modal/userfruits';
import {Usricecream} from '../modal/ice';
import { ProductInfo } from '../modal/prodata';
import {GetProductServices} from '../services/webproservices';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  isIn=false;
  items: ProductInfo;
  userfru: Fruitsuser;
  icc:Usricecream;
  search;
  constructor( private getproductservice:GetProductServices) { }

  ngOnInit() {
    this.getproductservice.pdrinfo()
    .subscribe((data)=>{
      this.items= data
    });

    this.getproductservice.fruit()
    .subscribe((data1)=>{
      this.userfru= data1
    });

    this.getproductservice.cream()
    .subscribe((data2)=>{
      this.icc= data2
    });
  }

  togglemenu()
  {
    let togggle=this.isIn;
    this.isIn=togggle===false? true : false;
  }

}
