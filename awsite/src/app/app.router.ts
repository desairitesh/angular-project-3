import {RouterModule , Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { ClothesComponent } from './clothes/clothes.component';
import { FoodComponent } from './food/food.component';
import { ShopingComponent } from './shoping/shoping.component';
import { ProductComponent } from './product/product.component';
import { Product2Component } from './product2/product2.component';
import { Product1Component } from './product1/product1.component';

export const routes = RouterModule.forRoot([
    
    {
        path:'',
        component:HomeComponent
    },
    {
        path:'home',
        component:HomeComponent
    },
    {
        path:'electronic',
        component:ElectronicComponent
    },
    {
        path:'food',
        component:FoodComponent
    },
    {
        path:'shoping',
        component:ShopingComponent
    },
    {
        path:'product/:productid',
        component:ProductComponent
    },
    {
        path:'product1/:productid',
        component:Product1Component
    },
    {
        path:'product2/:productid',
        component:Product2Component
    },
    {
        path:'clothes',
        component:ClothesComponent
    
}])