import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-electronic',
  templateUrl: './electronic.component.html',
  styleUrls: ['./electronic.component.css']
})
export class ElectronicComponent implements OnInit {

  isIn=false;
  constructor() { }

  ngOnInit() {
  }

  togglemenu()
 {
   let togggle=this.isIn;
   this.isIn=togggle===false? true : false;
 }

}
