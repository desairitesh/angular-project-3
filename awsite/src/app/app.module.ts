import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CarouselModule } from 'ngx-bootstrap';


import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeComponent } from './home/home.component';
import { FoodComponent } from './food/food.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { ClothesComponent } from './clothes/clothes.component';
import { SignupComponent } from './signup/signup.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { routes } from './app.router';
import { ShopingComponent } from './shoping/shoping.component';
import { GetProductServices } from './services/webproservices';
import { PaymentComponent } from './payment/payment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipe } from './filter.pipe';
import { Input, Output } from '@angular/core/src/metadata/directives';
import { ProductComponent } from './product/product.component';
import { Product1Component } from './product1/product1.component';
import { Product2Component } from './product2/product2.component';



@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    FoodComponent,
    ElectronicComponent,
    ClothesComponent,
    SignupComponent,
    FilterPipe,
    ShopingComponent,
    PaymentComponent,

    ProductComponent,
    Product1Component,
    Product2Component
  ],
  imports: [
    BrowserModule,
    CarouselModule.forRoot(),
    HttpModule,
    RouterModule,routes,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [GetProductServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
