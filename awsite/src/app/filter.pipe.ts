import { Pipe, PipeTransform } from '@angular/core';
import { ProductInfo } from './modal/prodata';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(value:ProductInfo[], arg:string) {
    if (!value)
    {
      return[];
    }
    if(!arg)
    {
      return value;
    }

   arg=arg.toLocaleLowerCase();

   return arg ? value.filter((pro:ProductInfo)=>pro.product.toLocaleLowerCase().includes(arg)) : value;
  }

}
