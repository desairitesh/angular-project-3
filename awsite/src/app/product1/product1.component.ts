import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GetProductServices} from '../services/webproservices';

@Component({
  selector: 'app-product1',
  templateUrl: './product1.component.html',
  styleUrls: ['./product1.component.css']
})
export class Product1Component implements OnInit {

  product1;
  constructor(private _route: ActivatedRoute, private getProduct: GetProductServices, private _router:Router) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      let id = params['productid'];
      this.getProduct.CurrentProduct1(id)
      .subscribe((items1) => {
       this.product1 = items1;
      })
  })
  }

  back1():void{
    this._router.navigate(["shoping"]);
 }


}
