export interface ProductInfo
{
    productid: number,
    product: string,
    code: string,
    available: string,
    price: number,
    ratings: number,
    ProductImage: string,
}