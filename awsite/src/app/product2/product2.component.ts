import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GetProductServices} from '../services/webproservices';

@Component({
  selector: 'app-product2',
  templateUrl: './product2.component.html',
  styleUrls: ['./product2.component.css']
})
export class Product2Component implements OnInit {

  product2;
  constructor(private _route: ActivatedRoute, private getProduct: GetProductServices, private _router:Router) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      let id = params['productid'];
      this.getProduct.CurrentProduct2(id)
      .subscribe((items2) => {
       this.product2 = items2;
      })
  })
  }

  back2():void{
    this._router.navigate(["food"]);
 }

}
