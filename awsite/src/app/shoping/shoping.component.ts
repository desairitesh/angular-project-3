import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductInfo } from '../modal/prodata';
import {GetProductServices} from '../services/webproservices';
import { FilterPipe } from '../filter.pipe';
import { Fruitsuser } from '../modal/userfruits';
import {Usricecream} from '../modal/ice';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-shoping',
  templateUrl: './shoping.component.html',
  styleUrls: ['./shoping.component.css']
})
export class ShopingComponent implements OnInit {

  isIn=false;
  items: ProductInfo;
  userfru: Fruitsuser;
  icc:Usricecream;
  search;
  constructor( private getproductservice:GetProductServices, private _router:Router) { }

  ngOnInit() {
    this.getproductservice.pdrinfo()
    .subscribe((data)=>{
      this.items= data
    });

    this.getproductservice.fruit()
    .subscribe((data1)=>{
      this.userfru= data1
    });

    this.getproductservice.cream()
    .subscribe((data2)=>{
      this.icc= data2
    });
  }

  togglemenu()
  {
    let togggle=this.isIn;
    this.isIn=togggle===false? true : false;
  }

 
}
