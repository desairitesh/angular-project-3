import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GetProductServices} from '../services/webproservices';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product;
    back1;
  constructor(private _route: ActivatedRoute, private getProduct: GetProductServices, private _router:Router) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      let id = params['productid'];
      this.getProduct.CurrentProduct(id)
      .subscribe((items) => {
       this.product = items;
      })
  })
  }

  back():void{
    this._router.navigate(["clothes"]);
 }

}
